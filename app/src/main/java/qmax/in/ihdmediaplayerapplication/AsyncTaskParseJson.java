package qmax.in.ihdmediaplayerapplication;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by surya on 2/11/18.
 */
public class AsyncTaskParseJson extends AsyncTask<String, String, String> {

    final String TAG = "AsyncTaskParseJson.java";

    String yourJsonStringUrl = "http://192.168.0.170/model_number.php";

    JSONArray dataJsonArr = null;

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected String doInBackground(String... arg0) {

        try {

            // instantiate our json parser
            JsonParser jParser = new JsonParser();

            // get json string from url
            JSONArray json = jParser.getJSONFromUrl(yourJsonStringUrl);


            for(int i=0;i<json.length();i++){
                JSONObject e = json.getJSONObject(i);
                String model_number = e.getString("model");
                Log.d(TAG,"Display model number"+model_number);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String strFromDoInBg) {
    }
}
