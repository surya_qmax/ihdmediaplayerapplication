package qmax.in.ihdmediaplayerapplication;

import android.Manifest;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener,
         SurfaceHolder.Callback, MediaPlayer.OnPreparedListener {

    VideoSurfaceView_MINIMX sv_minimx = null;
    Uri videofileplayurl;

    private static final int REQUEST_CODE_PERMISSION = 100 ;
    private MediaPlayer player;
    //private SurfaceView surface;
    private SurfaceHolder holder;
    private SongsManager songManager = null;
    private String playUrl = "";
    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
    private int i = 0;
    public File logf = null;
    int pos = 0;
    MarqueeLayout marqueeLayout;

    public TextView textView = null;
    public static  int present =0;

    public static String videoFileName = "";
    public static boolean videoFileRunning = false;
    public static String currentFilePlayed = "";

    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);



        setContentView(R.layout.activity_main);

        videoView = (VideoView) findViewById(R.id.surface);

        final TextView tv = (TextView) findViewById(R.id.textview1) ;


        final LinearLayout ln  = (LinearLayout) findViewById(R.id.imageView) ;

        tv.setText(Html.fromHtml("IHD INDUSTRIES PVT.LTD."+"\n"+"Inspection System"));


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Marshmallow+
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION

                    },
                    REQUEST_CODE_PERMISSION);

        } else {
            // Pre-Marshmallow
        }


       // new AsyncTaskParseJson().execute();


      /*  StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build();
        StrictMode.setThreadPolicy(policy);
*/
       final Handler handler = new Handler();

       final int delay = 1000; //milliseconds

        final int delaytwosec = 2000;
        handler.postDelayed(new Runnable(){

            public void run(){



                new BackgroundTask().execute();

                /*JSONObject reader = null;
                try {
                    reader = new JSONObject(readTwitterFeed);

                    JSONObject model_obj  = reader.getJSONObject("model_number");
                    String model_num = model_obj.getString("model_num");
                    Log.i("MMM",
                            "Data change" + model_num);
                    JSONArray jsonArray = new JSONArray(model_num);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject num = jsonArray.getJSONObject(i);
                        Log.i(MainActivity.class.getName(), "Display Model Number" + num.getString("model"));
                        videoFileName = num.getString("model");
                        Log.d("MMM","VideoFilename"+videoFileName);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }*/


               /* JSONObject jsonObject = null;
                JSONArray jsonArray = null;

                try {
                     jsonArray = new JSONArray(readTwitterFeed);
                    Log.i(JSONArray.class.getName(),
                            "Number of entries " + jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                         jsonObject = jsonArray.getJSONObject(i);
                        Log.i(MainActivity.class.getName(), "Display Model Number" + jsonObject.getString("model"));
                        videoFileName = jsonObject.getString("model");

                    }
                } catch (NullPointerException NPE)
                {
                    Log.i(MainActivity.class.getName(), "Display Model Number Error NPE" +NPE.getMessage().toString()+jsonArray.toString());

                } catch (Exception e) {
                    Log.i(MainActivity.class.getName(), "Display Model Number Error" +e.getMessage().toString()+jsonArray.toString());
                    e.printStackTrace();
                }
*/

                handler.postDelayed(this, delay);
            }
        }, delay);

       /* surface = (SurfaceView) findViewById(R.id.surface);*/
        songManager = new SongsManager();
        // Getting all songs list
        songsList = songManager.getPlayList();

        for(int i=0;i<songsList.size();i++)
        {
            String mediaListInPath = songsList.get(i).get("songPath");
            String mediaFileName =  songsList.get(i).get("songTitle");
            Log.d("MMMMM","MediaList file path"+mediaListInPath);
            Log.d("MMMMM","MediaList file path mediaFileName"+mediaFileName);


        }

        //String songListIn = songsList.get(0).toString();
        //Log.d("MMMM","Display songlistIn"+songListIn);
        /*holder = surface.getHolder();
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        holder.addCallback(this);*/
        File extDir = Environment.getExternalStorageDirectory();
        System.out.println("Long 1" + extDir);
        // File clip = new File(extDir+"/Movies/Coca-Cola.mp4");
        System.out.println("DSS index = " + i);
        // new BackgroundAsyncTask().execute(songsList.get(0).get("songPath"));
        //  marqueeLayout = new MarqueeLayout(this);

        //	viewFlipper = (ViewFlipper) findViewById(R.id.flipper);

        // textView.setSelected(true);





        handler.postDelayed(new Runnable(){

            public void run(){


                boolean videopresentinfolder = fileExistCheck();

                Log.d("mmmm","Display video file exists in folder read File name"+videopresentinfolder);

                Log.d("mmmm","Display video file current file played"+currentFilePlayed);


                if(isEmptyString(videoFileName))


                {


                    videoFileRunning = false;


                    currentFilePlayed = videoFileName;

                    Log.d("mmmm", "Empty  currentFilePlayed 1 ssss"+currentFilePlayed);


                    Log.d("mmmm","Display video file String file name empty read File name"+videoFileName);

                    if(player!=null) {


                        Log.d("mmmm","Display video file empty player not null");



                        player.stop();

                        player.reset();

                        player.release();

                        player = null;


                    }


                    videoView.setVisibility(View.GONE);

                    ln.setVisibility(View.VISIBLE);

                    tv.setVisibility(View.VISIBLE);
                }

                else if(videoFileName.equals("RESET"))
                {


                    Log.d("mmmm","Display video file String file name empty read File name"+videoFileName);

                    videoFileRunning = false;

                    currentFilePlayed = videoFileName;

                    Log.d("mmmm", "reset  currentFilePlayed 1 ssss"+currentFilePlayed);


                    if(player!=null) {


                       player.stop();

                       player.reset();


                       player.release();

                        player = null;


                    }

                    Log.d("MMMM","RESETTING THE PLAYER");


                    videoView.setVisibility(View.GONE);

                    ln.setVisibility(View.VISIBLE);

                    tv.setVisibility(View.VISIBLE);

                    tv.setText(Html.fromHtml("IHD INDUSTRIES PVT.LTD."+"\n"+"Inspection System"));
                }

                else if(videoFileName.toString().trim().length()!=5)
                {

                    videoFileRunning = false;

                    currentFilePlayed = videoFileName;


                    Log.d("mmmm", "length =-5  currentFilePlayed 1 ssss"+currentFilePlayed);

                    if(player!=null) {



                        player.stop();

                        player.release();

                        player = null;



                    }


                    Log.d("mmmm","Display video file String file name empty read File name"+videoFileName);


                    videoView.setVisibility(View.GONE);

                    ln.setVisibility(View.VISIBLE);

                    tv.setVisibility(View.VISIBLE);

                    tv.setText("Please Click on model number ");


                }


                else if(fileExistCheck())
                {

                    videoFileRunning = false;


                    currentFilePlayed = videoFileName;

                    if(player!=null) {

                        player.stop();


                        player.release();

                        player = null;



                    }
                    Log.d("mmmm", "file not exist   currentFilePlayed 1 ssss"+currentFilePlayed);




                    videoView.setVisibility(View.GONE);

                    ln.setVisibility(View.VISIBLE);

                    tv.setVisibility(View.VISIBLE);

                    tv.setText(videoFileName+": File Does Not Exist");
                }



                /*else if(!videoFileName.equals(currentFilePlayed))
                {



                    if (songsList.isEmpty())
                    {
                        Log.d("mmmm","Display empty view 444 current file not equalls");



                        if(player!=null) {

                            player.stop();

                            player.release();

                            player = null;



                        }



                        surface.setVisibility(View.GONE);

                        ln.setVisibility(View.VISIBLE);

                        tv.setVisibility(View.VISIBLE);


                    }
                    else {





                        ln.setVisibility(View.GONE);

                        tv.setVisibility(View.GONE);

                        surface.setVisibility(View.VISIBLE);



                        Log.d("mmmm","Display video view 2222");



                        for(int i=0;i<songsList.size();i++)
                        {
                            String mediaListInPath = songsList.get(i).get("songPath");
                            String mediaFileName =  songsList.get(i).get("songTitle");
                            Log.d("","MediaList file path"+mediaListInPath);
                            Log.d("MMMMM","MediaList file path mediaFileName"+mediaFileName);

                            if(videoFileName.equals(mediaFileName))
                            {
                                playVideo(songsList.get(i).get("songPath"));


                               *//*File source = new File(songsList.get(i).get("songPath"));



                                sv_minimx = new VideoSurfaceView_MINIMX(

                                        getApplicationContext(),

                                        songsList.get(i).get("songPath"),

                                        source,

                                        mediaFileName);
*//*

                                currentFilePlayed = mediaFileName;


                                present = 1;

                            }


                        }


                    }


                }

*/
                else if((videoFileName!=null
                        &&!videoFileRunning ) ) {



                    if (songsList.isEmpty())
                    {


                        if(player!=null) {

                            player.stop();


                            player.release();

                            player = null;



                        }


                        Log.d("mmmm","Display empty view 222 Folder Empty");

                        videoView.setVisibility(View.GONE);

                        ln.setVisibility(View.VISIBLE);

                        tv.setVisibility(View.VISIBLE);

                        tv.setText(videoFileName+": File Does Not Exist");



                    }
                    else {



                        ln.setVisibility(View.GONE);

                        tv.setVisibility(View.GONE);

                        videoView.setVisibility(View.VISIBLE);



                        Log.d("mmmm","Display video view 111");



                        for(int i=0;i<songsList.size();i++)
                        {
                            String mediaListInPath = songsList.get(i).get("songPath");
                            String mediaFileName =  songsList.get(i).get("songTitle");
                            Log.d("","Display video view 111 MediaList file path"+mediaListInPath);
                            Log.d("MMMMM","Display video view 111 MediaList file path mediaFileName"+mediaFileName);

                            if(videoFileName.equals(mediaFileName))
                            {
                               // playVideo(songsList.get(i).get("songPath"));




                                Uri uri=Uri.parse(songsList.get(i).get("songPath"));

                                //Setting MediaController and URI, then starting the videoView
                                videoView.setVideoURI(uri);
                                videoView.requestFocus();
                                videoView.start();
                                videoView.setOnPreparedListener (new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        mp.setLooping(true);
                                    }
                                });



                               /* File source = new File(songsList.get(i).get("songPath"));



                                sv_minimx = new VideoSurfaceView_MINIMX(

                                        getApplicationContext(),

                                        songsList.get(i).get("songPath"),

                                        source,

                                        mediaFileName);
*/

                                currentFilePlayed = videoFileName;

                                Log.d("mmmm", "videoview entered   currentFilePlayed 1 ssss"+currentFilePlayed);


                                present = 1;

                                videoFileRunning = true;

                                Log.d("MMMMM","Display video view 111 MediaList file path mediaFileName equals "+mediaFileName);



                            }


                        }


                    }


                }

                else
                {
                    Log.d("mmmm","Display empty view 1111"+videoFileRunning);

                    for(int i=0;i<songsList.size();i++) {
                        String mediaListInPath = songsList.get(i).get("songPath");
                        String mediaFileName = songsList.get(i).get("songTitle");
                        Log.d("", "MediaList file path" + mediaListInPath);
                        Log.d("MMMMM", "MediaList file path mediaFileName" + mediaFileName);

                        if (videoFileName.equals(mediaFileName)) {

                            Log.d("mmmm", "Keep Running");


                            Log.d("mmmm", "Keep Running received videofilename"+videoFileName);

                            Log.d("mmmm", "Keep Running  mediaFileName"+mediaFileName);

                            //  currentFilePlayed = mediaFileName;

                            Log.d("mmmm", "Keep Running  currentFilePlayed 2"+currentFilePlayed);


                            if(!videoFileName.equals(currentFilePlayed))
                            {
                                Log.d("mmmm", "Keep Running  currentFilePlayed 1"+currentFilePlayed);


                                Log.d("mmmm", "Keep Running1111");

                                //surface.setVisibility(View.VISIBLE);

                                ln.setVisibility(View.GONE);

                                tv.setVisibility(View.GONE);

                                currentFilePlayed = mediaFileName;

                               // playVideo(songsList.get(i).get("songPath"));





                                //specify the location of media file
                                Uri uri=Uri.parse(songsList.get(i).get("songPath"));

                                //Setting MediaController and URI, then starting the videoView

                                videoView.setVideoURI(uri);
                                videoView.requestFocus();
                                videoView.start();


                            }


                        }
                    }

                   // videoFileRunning = false;

                  /*  surface.setVisibility(View.GONE);

                    ln.setVisibility(View.VISIBLE);

                    tv.setVisibility(View.VISIBLE);*/





                }

                handler.postDelayed(this, delay);
            }
        }, delay);





    }



    public  boolean fileExistCheck()
    {

        for(int i=0;i<songsList.size();i++)
        {
            String mediaListInPath = songsList.get(i).get("songPath");
            String mediaFileName =  songsList.get(i).get("songTitle");
            Log.d("","MediaList file path"+mediaListInPath);
            Log.d("MMMMM","MediaList file path mediaFileName"+mediaFileName);

            if(videoFileName.equals(mediaFileName))
            {
                Log.d("MMMMM","MediaList file path mediaFileName"+mediaFileName);

                return false;

            }


        }

        return true;
    }

    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }

    public String readTwitterFeed() {
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("http://192.168.1.171/model.txt");
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }


            } else {
                Log.e(MainActivity.class.toString(), "Failed to download file");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }



    public void onCompletion(MediaPlayer arg0) {
        File dir = getCacheDir();



        Log.d("MMMM","MediaPlayer Completion called");

        for(int i = 0; i<songsList.size(); i++)
        {
            String mediaListInPath = songsList.get(i).get("songPath");
            String mediaFileName =  songsList.get(i).get("songTitle");
            Log.d("","Display video view 111 MediaList file path"+mediaListInPath);
            Log.d("MMMMM","Display video view 111 MediaList file path mediaFileName"+mediaFileName);

            if(videoFileName.equals(mediaFileName))
            {
             //   playVideo(songsList.get(i).get("songPath"));

                final int finalI = i;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        // new
                        // BackgroundAsyncTask().execute(songsList.get(i).get("songPath"));
                        playVideo(songsList.get(finalI).get("songPath"));


                    }
                }).start();

                currentFilePlayed = mediaFileName;

                present = 1;

                videoFileRunning = true;

                Log.d("MMMMM","Display video view 111 MediaList file path mediaFileName equals "+mediaFileName);



            }


        }




        // textView.setSelected(false);
/*
        for (int j = 0; j < songsList.size(); j++) {
            System.out.println("FOR DSS index = " + songsList.size()
                    + " current " + i);
            if (i < (songsList.size())) {
                System.out.println("FOR IF DSS index = " + i);
                //if(i!=0)
                i += 1;
                System.out.println("FOR DSS index completion= " + i
                        + " Song Path " + songsList.get(i).get("songPath"));
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        // new
                        // BackgroundAsyncTask().execute(songsList.get(i).get("songPath"));
                        playVideo(songsList.get(i).get("songPath"));

                        if (i == (songsList.size() - 1)) {
                            i = -1;
                        }
                    }
                }).start();
                break;
            }
        }*/
    }



    public void logcatDisplay() {
        try {
            File logcatFile = null;
            File logfile = Environment.getExternalStorageDirectory();
            logcatFile = new File(logfile + "/SVPLogcat.txt");
            if (!logcatFile.exists())
                logcatFile.createNewFile();
            Process process = Runtime.getRuntime().exec("logcat -d");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            StringBuilder logcat = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                logcat.append(line);
            }
            FileWriter fileWritter = new FileWriter(logcatFile, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(logcat.toString());
            bufferWritter.close();
        }
        catch (IOException e) {
        }
    }

    public void onPrepared(MediaPlayer mediaplayer) {
        try {
            holder.setFixedSize(player.getVideoWidth(), player.getVideoHeight());
            player.start();
            // textView.setSelected(true);


        } catch (Exception e) {

        }
    }

    private void playVideo(String url) {
        playUrl = url;
         videofileplayurl =  Uri.parse(url);
        try {
           if (player == null) {
                Log.d("MMMM","MediaPlayer ssss null called");

                player = new MediaPlayer();
                player.setScreenOnWhilePlaying(true);
                player.setWakeMode(getApplicationContext(),
                        PowerManager.PARTIAL_WAKE_LOCK);
                Log.d("mmm","TEST NEW PLAYER  NULL");

            } else {
                String currentDateTimeString = DateFormat.getDateTimeInstance()
                        .format(new Date());
                writeFile("\n Player End of Play :: " + currentDateTimeString);
                Log.d("MMMM","MediaPlayer ssss not null called");

                writeFile("\n --------------------------------------------------------------------");
                player.stop();
                player.reset();
                Log.d("mmm","TEST NEW PLAYER NOT NULL");

            }

            Log.d("MMMM","MediaPlayer ssss setdatasource called");


            player.setDataSource(getApplicationContext(), Uri.parse(url));
            player.setOnPreparedListener(MainActivity.this);
            player.prepareAsync();

//           player.setOnCompletionListener(MainActivity.this);
            // textView.setSelected(true);
        } catch (Throwable t) {
            Log.e("ERROR", "Exception Error", t);
            writeFile("\n Set The Player Exception Error :" + t.toString());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(player!= null) {
            player.stop();
            player.reset();
            player.release();
            player = null;
        }
        logcatDisplay();
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        // TODO Auto-generated method stub
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        try {
            Log.d("MMMM","MediaPlayer ssss surfacecreaed called"+videofileplayurl);

            player.setDisplay(holder);
        } catch (Exception e) {
            writeFile("\n Surface creatation error : " + e.toString());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {


        Log.d("MMMM","MediaPlayer ssss ondestroy called");


        // TODO Auto-generated method stub
    }

    public void writeFile(String log) {
        try {
            FileWriter fileWritter = new FileWriter(logf, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(log);
            bufferWritter.close();
        } catch (Exception e) {
        }
    }




    private class BackgroundTask extends AsyncTask<Void, Void, String> {

        /**
         * `doInBackground` is run on a separate, background thread
         * (not on the main/ui thread). DO NOT try to update the ui
         * from here.
         */
        @Override
        protected String doInBackground(Void... params) {

           String filename= readTwitterFeed();

            String readTwitterFeed = readTwitterFeed();
            Log.i("MMM",
                    "Data change" + readTwitterFeed);

            videoFileName  =readTwitterFeed;


            Log.d("MMMM",""+videoFileName);

            return videoFileName;
        }

        /**
         * `onPostExecute` is run after `doInBackground`, and it's
         * run on the main/ui thread, so you it's safe to update ui
         * components from it. (this is the correct way to update ui
         * components.)
         */
        @Override
        protected void onPostExecute(String param) {

        }
    }

}


