package qmax.in.ihdmediaplayerapplication;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;

import android.os.Environment;


public class SongsManager {
	// SDCard Path
	private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
	MainActivity main = new MainActivity();

	// Constructor
	public SongsManager() {
	}

	/**
	 * Function to read all mp3 files from sdcard and store the details in
	 * ArrayList
	 * */

	public ArrayList<HashMap<String, String>> getPlayList() {
		System.out.println("Long ");
		File home = null;
		try {
			String extDir = Environment.getExternalStorageDirectory()+"/IHD";
		    home = new File(extDir);
		  //  home = new File("/storage/external_storage/usb/usb1_1"+"/Test");
			//home = new File(extDir + "/Movies");
		//home = new File(extDir + "/DCIM/camera");
	//		System.out.println("aaa="+extDir);
		} catch (Exception e) {
			//main.writeFile("\n Log file Creation Error : " + e.toString());
		}
		try {
			if (home.listFiles(new FileExtensionFilter()).length > 0) {
				for (File file : home.listFiles(new FileExtensionFilter())) {
					HashMap<String, String> song = new HashMap<String, String>();
					song.put(
							"songTitle",
							file.getName().substring(0,
									(file.getName().length() - 4)));

				/*	if(file.getName().substring(0,
							(file.getName().length() - 4)).equalsIgnoreCase(MainActivity.videoFileName))
*/
					song.put("songPath", file.getPath());
					// Adding each song to SongList
					songsList.add(song);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			//main.writeFile("\n Get File From Directory " + e.toString());
		}
		// return songs list array
		return songsList;
	}

	/**
	 * Class to filter files which are having .mp3 extension
	 * */	

	
	class FileExtensionFilter implements FilenameFilter {
		public boolean accept(File dir, String name) {
			try {
				return (name.endsWith(".mp4") || name.endsWith(".MP4")
						|| name.endsWith(".mkv") || name.endsWith(".MKV")
						|| name.endsWith(".avi") || name.endsWith(".AVI")
						|| name.endsWith(".wmv") || name.endsWith(".WMV"));
			} catch (Exception e) {
				//main.writeFile("Get File Format Exception " + e.toString());
				return false;
			}
		}
	}
}
