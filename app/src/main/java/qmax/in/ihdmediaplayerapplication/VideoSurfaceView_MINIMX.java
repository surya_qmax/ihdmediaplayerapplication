package qmax.in.ihdmediaplayerapplication;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.File;



public class VideoSurfaceView_MINIMX extends SurfaceView {
	String TAG = "Video_MINIMX : ";
	SurfaceHolder surfaceHolder = null;
	MediaPlayer player = null;
	String VideoPath;
	Context context;
	File sourceG;
	String fNameAlone;
	boolean looping = false;
	boolean noSchedule = false;
	boolean reload = false;
	long screenDuration = 12345;
	Uri uri;

	/**
	 *
	 * This is a general video player code
	 *
	 * @param context
	 * @param fileName
	 * @param source
	 * @param fNameAlone

	 */
	public VideoSurfaceView_MINIMX(Context context, String fileName,
                                   File source, String fNameAlone) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.fNameAlone = fNameAlone;

		sourceG = source;
		VideoPath = fileName;
		this.looping = looping;
		surfaceHolder = getHolder();
		surfaceHolder.addCallback(new SurfaceCallback());
		this.noSchedule = noSchedule;
		Log.d(TAG,"Diplay uri download constructor called 1");
		initPlayer();

	}

	private void initPlayer() {
		Log.d(TAG,"Diplay init player called 1");

		try {
		try {


						uri = Uri.parse(VideoPath);

						player = MediaPlayer.create(context, uri);
					} catch (Exception e) {
						player = null;
					}

					try {
						playVideo(VideoPath);

					} catch (Exception e) {
						e.printStackTrace();
					}


				if (player != null) {
					if (looping)
						looping = true;
					else
						looping = false;

					player.setLooping(looping);


			}

		} catch (IllegalStateException e) {
			print("IllegalStateException: " + e.getMessage());
		} catch (IllegalArgumentException e) {
			print("IllegalArgumentException: " + e.getMessage());
		} catch (SecurityException e) {
			print("SecurityException: " + e.getMessage());
		} catch (Exception e) {
			print("SecurityException: " + e.getMessage());

		}
	}

	Handler h = new Handler();
	Runnable r = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (reload) {
				Log.d(TAG,"Enter into relead");
				initPlayer();
			}
			if (player != null) {
				Log.d(TAG,"Enter into player not null in handler");
				try {
					Log.d(TAG,"Enter into player not null in handler try");
					player.setDisplay(surfaceHolder);
					player.setLooping(looping);
					player.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};

	public long getDuration() {

		try {
			Uri uri = Uri.parse(VideoPath);
			MediaPlayer tempPlayer = MediaPlayer.create(context, uri);
			screenDuration = tempPlayer.getDuration() + 500;
			tempPlayer.reset();
			tempPlayer.release();
		} catch (Exception e) {
			screenDuration = 10000;
		}
		return screenDuration;
	}

	private final class SurfaceCallback implements SurfaceHolder.Callback {

		public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {
		}

		public void surfaceCreated(SurfaceHolder holder) {
			print("onCReated");

			try {
				h.postDelayed(r, 100);
				Log.d(TAG,"surface created try called");

			} catch (Exception e) {
				print("surfacecallllback " + e.toString() + " for file "
						+ VideoPath);
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder) {

			if (player.isPlaying()) {
				print("Destroyed");
				player.stop();
				player.reset();
				player.release();
				reload = true;
				player = null;
			}
			print("Destroyed2");

		}

	}

	public void print(String print) {
		System.out.println(TAG + print);
	}

	private void playVideo(String url) {

		String mplay_path = url;
		try {

			player.setDataSource(mplay_path);
			player.setOnPreparedListener(new OnPreparedListener() {

				@Override
				public void onPrepared(MediaPlayer mp) {
					// TODO Auto-generated method stub
					try {
						surfaceHolder.setFixedSize(player.getVideoWidth(),
								player.getVideoHeight());
						player.start();

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			player.prepareAsync();
			player.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					// TODO Auto-generated method stub
				}
			});
		} catch (Throwable t) {
			Log.e("ERROR", "Exception Error", t);
		}
	}

}